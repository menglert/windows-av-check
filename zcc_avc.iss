#define MyAppName "Zscaler Client Connector with AV Check"
#define MyAppVersion "2.1.2.105_0.1"
#define MyAppPublisher "Zscaler Inc."
#define MyAppURL "http://www.zscaler.com/"
#define MyAppExeName "zcc_avc.exe"

[Setup]
AppId={{16A416B0-1248-45F0-B328-C21AFFCDECB0}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
CreateAppDir=yes
DisableDirPage=yes
DefaultDirName={autopf32}\Zscaler
Compression=lzma
SolidCompression=yes
WizardStyle=modern

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Files]
Source: "C:\Users\michi\Downloads\Zscaler-windows-2.1.2.105-installer.exe"; DestDir: "{tmp}"; Flags: ignoreversion
Source: "C:\Users\michi\Source\Repos\CheckAV\Release\ZSCheckAV.exe"; DestDir: "{app}"; Flags: ignoreversion

[run]
Filename: {sys}\sc.exe; Parameters: "create ZscalerAVCheck start= auto binPath= ""{app}\ZSCheckAV.exe"" DisplayName=""Zscaler AV Check""" ; Flags: runhidden
Filename: {sys}\sc.exe; Parameters: "start ZscalerAVCheck" ; Flags: runhidden
Filename: {tmp}\Zscaler-windows-2.1.2.105-installer.exe; Parameters: "--cloudName <CLOUDNAME> --mode unattended --userDomain <USERDOMAIN>" ; Flags: runhidden

[UninstallRun]
Filename: {sys}\sc.exe; Parameters: "stop ZscalerAVCheck" ; Flags: runhidden
Filename: {sys}\sc.exe; Parameters: "delete ZscalerAVCheck" ; Flags: runhidden
Filename: {app}\ZSAInstaller\uninstall.exe; Parameters: "--mode unattended"

[UninstallDelete]
Type: filesandordirs; Name: "{app}"