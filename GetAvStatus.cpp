#include <windows.h>
#include <tchar.h>
#include <strsafe.h>
#include <stdio.h>
#include <atlbase.h>
#include <atlstr.h>
#include <wscapi.h>
#include <iwscapi.h>

#pragma comment(lib, "advapi32.lib")

SERVICE_STATUS        g_ServiceStatus = { 0 };
SERVICE_STATUS_HANDLE g_StatusHandle = NULL;
HANDLE                g_ServiceStopEvent = INVALID_HANDLE_VALUE;

VOID WINAPI ServiceMain(DWORD argc, LPTSTR* argv);
VOID WINAPI ServiceCtrlHandler(DWORD);
DWORD WINAPI ServiceWorkerThread(LPVOID lpParam);

#define SERVICE_NAME  _T("Zscaler AVCheck")   


HRESULT GetSecurityProducts(WSC_SECURITY_PROVIDER provider)
{
    HRESULT hr = S_OK;
    IWscProduct* PtrProduct = nullptr;
    IWSCProductList* PtrProductList = nullptr;
    BSTR PtrVal = nullptr;
    LONG ProductCount = 0;
    WSC_SECURITY_PRODUCT_STATE ProductState;
    WSC_SECURITY_SIGNATURE_STATUS ProductStatus;
    HKEY hkey = NULL;
    LPCWSTR strKey = L"Software\\Zscaler\\AV_Check";
    LONG nError = RegOpenKeyEx(HKEY_CURRENT_USER, strKey, 0, KEY_ALL_ACCESS, &hkey);

    if (nError != ERROR_SUCCESS)
    {
        wprintf(L"Registry Key %s not there try creating", strKey);
        RegCreateKeyEx(HKEY_CURRENT_USER, strKey, NULL, NULL, REG_OPTION_VOLATILE, KEY_ALL_ACCESS, NULL, &hkey, NULL);
    }

    hr = CoCreateInstance(
        __uuidof(WSCProductList),
        NULL,
        CLSCTX_INPROC_SERVER,
        __uuidof(IWSCProductList),
        reinterpret_cast<LPVOID*> (&PtrProductList));

    if (FAILED(hr))
    {
        wprintf(L"CoCreateInstance returned error = 0x%d \n", hr);
        goto exit;
    }

    hr = PtrProductList->Initialize(provider);
    if (FAILED(hr))
    {
        wprintf(L"Initialize failed with error: 0x%d\n", hr);
        goto exit;
    }

    hr = PtrProductList->get_Count(&ProductCount);
    if (FAILED(hr))
    {
        wprintf(L"get_Count failed with error: 0x%d\n", hr);
        goto exit;
    }

    for (LONG i = 0; i < ProductCount; i++)
    {
        hr = PtrProductList->get_Item(i, &PtrProduct);
        if (FAILED(hr))
        {
            wprintf(L"get_Item failed with error: 0x%d\n", hr);
            goto exit;
        }

        hr = PtrProduct->get_ProductName(&PtrVal);
        if (FAILED(hr))
        {
            wprintf(L"get_ProductName failed with error: 0x%d\n", hr);
            goto exit;
        }

        nError = RegSetValueEx(hkey, TEXT("Name"), NULL, REG_SZ, (BYTE*)PtrVal, wcslen(PtrVal) * sizeof(TCHAR));

        if (nError != ERROR_SUCCESS)
        {
            wprintf(L"Error setting Registry value for Signatures %s.", PtrVal);
        }

        SysFreeString(PtrVal);
        PtrVal = nullptr;

        hr = PtrProduct->get_ProductState(&ProductState);
        if (FAILED(hr))
        {
            wprintf(L"get_ProductState failed with error: 0x%d\n", hr);
            goto exit;
        }

        LPCWSTR pszState;
        if (ProductState == WSC_SECURITY_PRODUCT_STATE_ON)
        {
            pszState = L"On";
        }
        else if (ProductState == WSC_SECURITY_PRODUCT_STATE_OFF)
        {
            pszState = L"Off";
        }
        else if (ProductState == WSC_SECURITY_PRODUCT_STATE_SNOOZED)
        {
            pszState = L"Snoozed";
        }
        else
        {
            pszState = L"Expired";
        }

        nError = RegSetValueEx(hkey, TEXT("State"), NULL, REG_SZ, (BYTE*)pszState, wcslen(pszState) * sizeof(TCHAR));

        if (nError != ERROR_SUCCESS)
        {
            wprintf(L"Error setting Registry value for Signatures %s.", pszState);
        }

        hr = PtrProduct->get_SignatureStatus(&ProductStatus);
        if (FAILED(hr))
        {
            wprintf(L"get_SignatureStatus failed with error: 0x%d\n", hr);
            goto exit;
        }
        LPCWSTR pszStatus = (ProductStatus == WSC_SECURITY_PRODUCT_UP_TO_DATE) ?
            L"Up-to-date" : L"Out-of-date";

        nError = RegSetValueEx(hkey, TEXT("Signatures"), NULL, REG_SZ, (BYTE*)pszStatus, wcslen(pszStatus) * sizeof(TCHAR));

        if (nError != ERROR_SUCCESS)
        {
            wprintf(L"Error setting Registry value for Signatures %s.", pszStatus);
        }

        PtrProduct->Release();
        PtrProduct = nullptr;

        LONG closeOut = RegCloseKey(hkey);

        if (closeOut != ERROR_SUCCESS) {
            wprintf(L"Error closing Registry key.");
        }
    }

exit:

    if (nullptr != PtrVal)
    {
        SysFreeString(PtrVal);
    }
    if (nullptr != PtrProductList)
    {
        PtrProductList->Release();
    }
    if (nullptr != PtrProduct)
    {
        PtrProduct->Release();
    }
    return hr;
}

int _tmain(int argc, TCHAR* argv[])
{
    SERVICE_TABLE_ENTRY ServiceTable[] =
    {
        {(LPWSTR) SERVICE_NAME, (LPSERVICE_MAIN_FUNCTION)ServiceMain},
        {NULL, NULL}
    };

    if (StartServiceCtrlDispatcher(ServiceTable) == FALSE)
    {
        return GetLastError();
    }

    return 0;
}

VOID WINAPI ServiceMain(DWORD argc, LPTSTR* argv)
{
    DWORD Status = E_FAIL;
    HANDLE hThread;

    g_StatusHandle = RegisterServiceCtrlHandler(SERVICE_NAME, ServiceCtrlHandler);

    if (g_StatusHandle == NULL)
    {
        goto EXIT;
    }

    ZeroMemory(&g_ServiceStatus, sizeof(g_ServiceStatus));
    g_ServiceStatus.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
    g_ServiceStatus.dwControlsAccepted = 0;
    g_ServiceStatus.dwCurrentState = SERVICE_START_PENDING;
    g_ServiceStatus.dwWin32ExitCode = 0;
    g_ServiceStatus.dwServiceSpecificExitCode = 0;
    g_ServiceStatus.dwCheckPoint = 0;

    if (SetServiceStatus(g_StatusHandle, &g_ServiceStatus) == FALSE)
    {
        OutputDebugString(_T(
            "Zscaler AVCheck: SetServiceStatus returned error"));
    }

    g_ServiceStopEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (g_ServiceStopEvent == NULL)
    {
        g_ServiceStatus.dwControlsAccepted = 0;
        g_ServiceStatus.dwCurrentState = SERVICE_STOPPED;
        g_ServiceStatus.dwWin32ExitCode = GetLastError();
        g_ServiceStatus.dwCheckPoint = 1;

        if (SetServiceStatus(g_StatusHandle, &g_ServiceStatus) == FALSE)
        {
            OutputDebugString(_T(
                "Zscaler AVCheck: SetServiceStatus returned error"));
        }
        goto EXIT;
    }

    g_ServiceStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP;
    g_ServiceStatus.dwCurrentState = SERVICE_RUNNING;
    g_ServiceStatus.dwWin32ExitCode = 0;
    g_ServiceStatus.dwCheckPoint = 0;

    if (SetServiceStatus(g_StatusHandle, &g_ServiceStatus) == FALSE)
    {
        OutputDebugString(_T(
            "Zscaler AVCheck: SetServiceStatus returned error"));
    }
     
    hThread = CreateThread(NULL, 0, ServiceWorkerThread, NULL, 0, NULL);

    WaitForSingleObject(hThread, INFINITE);

    CloseHandle(g_ServiceStopEvent);

    g_ServiceStatus.dwControlsAccepted = 0;
    g_ServiceStatus.dwCurrentState = SERVICE_STOPPED;
    g_ServiceStatus.dwWin32ExitCode = 0;
    g_ServiceStatus.dwCheckPoint = 3;

    if (SetServiceStatus(g_StatusHandle, &g_ServiceStatus) == FALSE)
    {
        OutputDebugString(_T(
            "Zscaler AVCheck: SetServiceStatus returned error"));
    }

EXIT:
    return;
}

VOID WINAPI ServiceCtrlHandler(DWORD CtrlCode)
{
    switch (CtrlCode)
    {
    case SERVICE_CONTROL_STOP:

        if (g_ServiceStatus.dwCurrentState != SERVICE_RUNNING)
            break;

        g_ServiceStatus.dwControlsAccepted = 0;
        g_ServiceStatus.dwCurrentState = SERVICE_STOP_PENDING;
        g_ServiceStatus.dwWin32ExitCode = 0;
        g_ServiceStatus.dwCheckPoint = 4;

        if (SetServiceStatus(g_StatusHandle, &g_ServiceStatus) == FALSE)
        {
            OutputDebugString(_T(
                "Zscaler AVCheck: SetServiceStatus returned error"));
        }


        SetEvent(g_ServiceStopEvent);

        break;

    default:
        break;
    }
}

DWORD WINAPI ServiceWorkerThread(LPVOID lpParam)
{
    HRESULT hr = S_OK;

    while (WaitForSingleObject(g_ServiceStopEvent, 0) != WAIT_OBJECT_0)
    {
        CoInitializeEx(0, COINIT_APARTMENTTHREADED);

        hr = GetSecurityProducts(WSC_SECURITY_PROVIDER_ANTIVIRUS);

        CoUninitialize();
        Sleep(60000);
    }

    return ERROR_SUCCESS;
}